package com.mercadolivro.mercadolivro.config

import io.swagger.v3.oas.models.info.Info
import org.springdoc.core.models.GroupedOpenApi
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
class SwaggerConfig {

    @Bean
    fun publicApi(): GroupedOpenApi {
        return GroupedOpenApi.builder()
            .group("public-api")
            .pathsToMatch("/**")
            .packagesToScan("com.mercadolivro.controller")
            .build()
    }

    @Bean
    fun apiInfo(): Info {
        return Info()
            .title("Mercado Livro")
            .description("Api do Mercado Livro")
    }

}